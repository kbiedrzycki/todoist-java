package pl.acme;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Todoist2Tests {
    @Test
    public void verifyPageTitle() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--single-process");
        options.addArguments("--use-gl=swiftshader");
        options.addArguments("--no-zygote");

        WebDriver browser = new ChromeDriver(options);
        browser.get(System.getProperty("inputParameters.testTargetURL"));

        String title = browser.getTitle();


        assertTrue((title.contentEquals("Inbox: Todoist")));

        browser.close();
    }
}
