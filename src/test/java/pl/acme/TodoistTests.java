package pl.acme;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class TodoistTests {
    @Test
    public void verifyPageTitle() throws IOException {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--single-process");
        options.addArguments("--use-gl=swiftshader");
        options.addArguments("--no-zygote");

        WebDriver browser = new ChromeDriver(options);
        browser.get(System.getProperty("inputParameters.testTargetURL"));

        String title = browser.getTitle();
        System.out.println(title);

        assertTrue((title.contentEquals("Inbox: Todoist")));

        File file = ((TakesScreenshot) browser).getScreenshotAs(OutputType.FILE);
        String filePath = System.getProperty("ARTIFACTS_PATH") + "/verify-page-title-test-1.png";
        System.out.println("filePath: " + filePath);

        FileUtils.copyFile(file, new File(filePath));
        
        System.setProperty("output", String.format("{\"page\":{\"title\":\"%s\"}}", title));

        browser.close();
    }
}
